import pandas as pd
import re

file_path = './Dataset/wine-reviews.csv'
df = pd.read_csv(file_path)

# Extract vintage year from title and drop reviews without one
year_pattern = r'(\b\d{4}\b)'
df['vintage_year'] = df['title'].str.extract(year_pattern)

# Deduce
# Display null values before any changes
print("\nNull values in each column before any changes:")
print(df.isnull().sum())

# Remove unused columns
df = df.drop(
    ['description', 'region_1', 'region_2', 'taster_name', 'taster_twitter_handle', 'title'], axis=1)

# Drop rows where essential columns are null
to_drop_na = ['designation', 'variety', 'price',
              'country', 'province', 'vintage_year']
for todrop in to_drop_na:
    len_before = len(df)
    df = df.dropna(subset=[todrop])
    print(
        f"Dropped Null values for {todrop}, found {len_before - len(df)} NA values")

# Display null values after changes
print("\nNull values in each column after changes:")
print(df.isnull().sum())

# Remove first unnamed column
df = df.iloc[:, 1:]

# Extract wine color informations
# Manual treatment
variety_counts = df['variety'].value_counts()
top_100_varieties = variety_counts.head(100)
output_file_path = './logs/top_100_varieties.txt'
top_100_varieties.to_csv(output_file_path, header=True)

# Result of AI labor
reduced_wine_colors = {
    'Red': ['Pinot Noir', 'Red Blend', 'Cabernet Sauvignon', 'Syrah', 'Bordeaux-style Red Blend', 'Portuguese Red', 'Zinfandel', 'Malbec', 'Nebbiolo', 'Merlot', 'Tempranillo', 'Sangiovese', 'Rhône-style Red Blend', 'Cabernet Franc', 'Gamay', 'Shiraz', 'Tempranillo Blend', 'Carmenère', 'Grenache', 'Petite Sirah', 'Barbera', 'Sangiovese Grosso', 'Nero d\'Avola', 'Garnacha', 'Aglianico', 'Petit Verdot', 'G-S-M', 'Mourvèdre', 'Montepulciano', 'Primitivo', 'Tinta de Toro', 'Meritage', 'Blaufränkisch', 'Tannat', 'Zweigelt', 'Mencía', 'Monastrell', 'Touriga Nacional', 'Pinot Nero', 'Nerello Mascalese', 'Dolcetto', 'Cabernet Sauvignon-Syrah', 'Cabernet Sauvignon-Merlot', 'Cabernet Blend', 'Malbec-Merlot', 'Spätburgunder', 'Carignan', 'Negroamaro', 'Malbec-Cabernet Sauvignon', 'Lagrein', 'St. Laurent', 'Sagrantino', 'Austrian Red Blend'],
    'White': ['Chardonnay', 'Riesling', 'Sauvignon Blanc', 'Grüner Veltliner', 'Portuguese White', 'White Blend', 'Chenin Blanc', 'Pinot Gris', 'Gewürztraminer', 'Viognier', 'Glera', 'Vermentino', 'Verdicchio', 'Chardonnay-Semillon', 'Muscat Blanc à Petits Grains', 'Sauvignon Blanc-Chardonnay', 'Semillon-Sauvignon Blanc', 'Sauvignon Blanc-Semillon', 'Sparkling Blend', 'Rhône-style White Blend', 'Pinot Blanc', 'Albariño', 'Pinot Bianco', 'Viura', 'Chardonnay-Viognier', 'Furmint', 'Alvarinho', 'Grenache Blanc', 'Fiano', 'Grillo', 'Vernaccia', 'Viura-Chardonnay', 'Chardonnay-Sauvignon Blanc', 'Assyrtiko', 'Colombard', 'Verdejo', 'Austrian white blend', 'Sauvignon Blanc-Semillon'],
    'Rosé': ['Rosé']
}

# Create color column
df['color'] = df['variety'].map(
    {variety: color for color, varieties in reduced_wine_colors.items() for variety in varieties})

# Second pass with string analysis


def extract_color(variety):
    if 'red' in variety.lower():
        return 'red'
    elif 'rosé' in variety.lower():
        return 'rosé'
    elif 'white' in variety.lower():
        return 'white'
    elif 'champagne' in variety.lower():
        return 'champagne'
    else:
        return None


missing_color_mask = df['color'].isna()
df.loc[missing_color_mask, 'color'] = df.loc[missing_color_mask,
                                             'variety'].apply(extract_color)
df = df.dropna(subset=['color'])

# Fix illegal rdf characters
forbidden_characters = set("'/\"\#%!?:;([)]\{\}=\"")
mask = df['designation'].str.contains(
    '|'.join(map(re.escape, forbidden_characters)))
df = df[~mask]
mask = df['variety'].str.contains(
    '|'.join(map(re.escape, forbidden_characters)))
df = df[~mask]
mask = df['winery'].str.contains(
    '|'.join(map(re.escape, forbidden_characters)))
df = df[~mask]

# Gather info on df
print("\nStatistics about the DataFrame:")
print(df.describe())
print(f'Number of Rows : {len(df)}')
print("Number of unique values in each column:")
print(df.nunique())
print("Datatypes schema :")
print(df.dtypes)

# Create output csv
output_file_path = './Dataset/wine-reviews-cleaned.csv'
df.to_csv(output_file_path, index=True, index_label="review_id")
print(
    f"\nCleaned DataFrame exported to {output_file_path} and to java project")
