# CSC5003 - Project

# Members

Devan Prigent
Malek Hammou
Jules Risse

# Semantic Web

This is a school project on semantic web technologies, we opted to choose a pre-existing ontology and integrate it with a dataset. Inspired by the food ontologies we could see online, we decided to study wines.

Based on :
wine ontology ( https://lov.linkeddata.es/dataset/lov/vocabs/vin )
wine review dataset ( https://www.kaggle.com/datasets/zynicide/wine-reviews )

This repository contains our modified version of the ontology, the cleaned dataset made to fit insertion inside the ontology, as well as the code to do so.
The python code under /src/python-utils/ contain the script to generate the cleaned up csv file.
The java project under /src/semantic-web-project/ is built with Maven and contains the ontology populating logic as well as SPARQL queries.

## How To use

Requirements :
Python > 3.8 with pandas
Java with Maven
Protégé (for viewing ontology)

# Create cleaned dataset

python3 ./src/python-utils/clean-data.py

# Populate ontology

./exec_java.sh
