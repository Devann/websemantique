package CSC5003;

import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.ontology.OntProperty;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QueryParseException;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.util.iterator.ExtendedIterator;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Ontology populating and queries
 */
public class App {

    public static final String MY_NAMESPACE = "http://www.semanticweb.org/jules/ontologies/2024/1/untitled-ontology-27/";

    /**
     * Remove properties with null URI values.
     * 
     * @param properties the property iterator
     * @return the number of cleaned elements
     */
    private static int cleanProperties(ExtendedIterator<? extends OntProperty> properties) {
        int counter = 0;
        while (properties.hasNext()) {
            OntProperty property = properties.next();
            if (property.getURI() == null) {
                property.remove();
                counter++;
            }
        }
        return counter;
    }

    /**
     * Remove classes with null URI values.
     * 
     * @param classes the class iterator
     * @return the number of classes cleaned
     */
    private static int cleanClasses(ExtendedIterator<OntClass> classes) {
        int counter = 0;
        while (classes.hasNext()) {
            OntClass ontClass = classes.next();
            if (ontClass.getURI() == null) {
                classes.remove();
                counter++;
            }
        }
        return counter;
    }

    /**
     * Clean null URI's from the model.
     * 
     * @param model the Ontology Model
     */
    public static void cleanNullURIs(OntModel model) {
        int counter_classes = cleanClasses(model.listClasses());
        System.out.println("Removed " + counter_classes + " Null Classes entries");
        int counter_object_properties = cleanProperties(model.listObjectProperties());
        System.out.println("Removed " + counter_object_properties + " Null Object Properties entries");
        int counter_datatype_properties = cleanProperties(model.listDatatypeProperties());
        System.out.println("Removed " + counter_datatype_properties + " Null Object Properties entries");
        ExtendedIterator<Individual> individuals = model.listIndividuals();
        int counter_individuals = 0;
        while (individuals.hasNext()) {
            Individual individual = individuals.next();
            if (individual.getURI() == null) {
                individual.remove();
                counter_individuals++;
            }
        }
        System.out.println("Removed " + counter_individuals + " Individuals entries");
    }

    /**
     * Show classes of the model.
     * 
     * @param model the model
     */
    static void getClassInformations(OntModel model) {
        OntClass ontClass;
        ExtendedIterator<OntClass> classes = model.listClasses();
        while (classes.hasNext()) {
            ontClass = classes.next();
            if (ontClass.getURI() != null) {
                System.out.println("Class URI: " + ontClass.getURI());
                System.out.println("Class Local Name: " + ontClass.getLocalName());
                System.out.println("Class Label: " + ontClass.getLabel("en"));
                System.out.println("---");
            }

        }
    }

    /**
     * Read data from a CSV file and return a list of objects.
     *
     * @param csvFilePath the path to the CSV file
     * @return a list of objects representing the CSV data
     */
    private static List<String[]> readCsv(String csvFilePath) {
        List<String[]> csvData = new ArrayList<>();

        try (InputStream inputStream = App.class.getResourceAsStream(csvFilePath);
                CSVReader reader = new CSVReader(new InputStreamReader(inputStream))) {

            if (inputStream == null) {
                throw new FileNotFoundException("Resource not found: " + csvFilePath);
            }

            csvData = reader.readAll();
        } catch (IOException | CsvException e) {
            e.printStackTrace();
        }

        return csvData;
    }

    /**
     * Write the ontology model to a file.
     *
     * @param model   the ontology model
     * @param outFile the path to the output file
     */
    private static void writeModelToFile(OntModel model, String outFile) {
        try {
            Path filePath = Paths.get(outFile);
            if (!Files.exists(filePath)) {
                Files.createFile(filePath);
            }

            // Write the model to the file
            try (OutputStream outputStream = new FileOutputStream(outFile)) {
                model.write(outputStream, "RDF/XML-ABBREV");
                System.out.println("Modified ontology written to: " + outFile);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create individuals, and establish object property relationships.
     *
     * @param csvFilePath the path to the CSV file
     * @param model       the ontology model
     */
    private static void InsertIndividualsFromCsv(List<String[]> csvData, OntModel model) {
        if (!csvData.isEmpty()) {
            // int end_index = csvData.size();
            int end_index = 30000;
            for (int i = 1; i < end_index; i++) {
                String[] record = csvData.get(i);
                System.out.println("Populating row n°" + i);

                // Create class individuals
                Resource wineIndividual = createIndividual(model, "Wine", record[0]);
                Resource reviewIndividual = createIndividual(model, "Review", record[0]);
                Resource countryIndividual = createIndividual(model, "Country", record[1]);
                Resource designationIndividual = createIndividual(model, "Designation", record[2]);
                Resource pointsIndividual = createIndividual(model, "Score", record[3]);
                Resource priceIndividual = createIndividual(model, "Price", record[4]);
                Resource provinceIndividual = createIndividual(model, "Province", record[5]);
                Resource varietyIndividual = createIndividual(model, "Variety", record[6]);
                Resource wineryIndividual = createIndividual(model, "Winery", record[7]);
                Resource vintageIndividual = createIndividual(model, "Vintage", record[8]);
                Resource colorIndividual = createIndividual(model, "Color", record[9]);

                // Create object property relationships
                createObjectProperty(model, wineIndividual, "isReviewedIn", reviewIndividual);
                createObjectProperty(model, wineIndividual, "hasCountry", countryIndividual);
                createObjectProperty(model, wineIndividual, "hasDesignation", designationIndividual);
                createObjectProperty(model, wineIndividual, "hasScore", pointsIndividual);
                createObjectProperty(model, wineIndividual, "hasPrice", priceIndividual);
                createObjectProperty(model, wineIndividual, "hasProvince", provinceIndividual);
                createObjectProperty(model, wineIndividual, "hasVariety", varietyIndividual);
                createObjectProperty(model, wineIndividual, "hasMaker", wineryIndividual);
                createObjectProperty(model, wineIndividual, "hasVintage", vintageIndividual);
                createObjectProperty(model, wineIndividual, "hasColor", colorIndividual);
            }
        }
    }

    /**
     * Create an individual of a specific class in the ontology if it doesn't
     * already exist.
     *
     * @param model          the ontology model
     * @param className      the rdf:label of the EXISTING class
     * @param individualName the name of the individual
     * @return the existing or newly created individual
     */
    private static Resource createIndividual(OntModel model, String className, String individualName) {
        individualName = individualName.replace("\"", " ").replace("'", " ");
        ExtendedIterator<OntClass> classes = model.listClasses();
        OntClass ontClass = null;

        while (classes.hasNext()) {
            OntClass candidate = classes.next();
            if (className.equals(candidate.getLabel(null))) {
                ontClass = candidate;
                break;
            }
        }

        if (ontClass == null) {
            System.out.println("Error: Class not found in the ontology: " + className);
            return null;
        }

        String individualURI = MY_NAMESPACE + className + "_" + individualName.replace(" ", "_");

        // Check if the individual already exists
        Resource existingIndividual = model.getResource(individualURI);
        if (existingIndividual != null && existingIndividual.hasProperty(model.getProperty("rdf:type"), ontClass)) {
            return existingIndividual;
        }

        // Create a new individual if it doesn't exist
        Resource individual = model.createIndividual(individualURI, ontClass);
        return individual;
    }

    /**
     * Create an object property relationship between two individuals in the
     * ontology.
     *
     * @param model        the ontology model
     * @param subject      the subject individual
     * @param propertyName the rdf:label of the EXISTING object property
     * @param object       the object individual
     */
    private static void createObjectProperty(OntModel model, Resource subject, String propertyName, Resource object) {
        ExtendedIterator<OntProperty> properties = model.listAllOntProperties();
        OntProperty property = null;
        while (properties.hasNext()) {
            OntProperty candidate = properties.next();
            if (propertyName.equals(candidate.getLabel(null))) {
                property = candidate;
                break;
            }
        }
        if (property != null) {
            model.add(subject, property, object);
        } else {
            System.out.println("Error: Object property not found in the ontology: " + propertyName);
        }
    }

    private static void executeSparqlQuery(OntModel model) {
        String sparqlQuery = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "PREFIX owl: <http://www.w3.org/2002/07/owl#>\n" +
                "PREFIX : <http://www.semanticweb.org/jules/ontologies/2024/1/untitled-ontology-27#>\n" +
                "\n" +
                "SELECT ?wine ?score ?price ?designation \n" +
                "WHERE {\n" +
                "  ?wine rdf:type :Wine ;\n" +
                "        :hasScore ?score ;\n" +
                "        :hasPrice ?price ;\n" +
                "        :hasDesignation ?designation .\n" +
                "}\n" +
                "ORDER BY DESC(?score/?price)\n" +
                "LIMIT 10";
        Query query = QueryFactory.create(sparqlQuery);
        try (QueryExecution queryExecution = QueryExecutionFactory.create(query, model)) {
            ResultSet resultSet = queryExecution.execSelect();
            ResultSetFormatter.out(System.out, resultSet, query);
        } catch (QueryParseException e) {
            System.out.println("Error in SPARQL query: " + e.getMessage());
        }
    }

    public static void main(String[] args) {

        // Load ontology
        OntModel model = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM);
        String ontologyFile = "/wine.rdf";

        try (InputStream inputStream = App.class.getResourceAsStream(ontologyFile)) {
            RDFDataMgr.read(model, inputStream, Lang.RDFXML);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Clean model
        cleanNullURIs(model);

        // Open CSV file
        String csvFilePath = "/wine-reviews-cleaned.csv";
        List<String[]> csvData = readCsv(csvFilePath);

        // Populate model
        InsertIndividualsFromCsv(csvData, model);

        // Execute SPARQL
        executeSparqlQuery(model);

        // Save the modified ontology to a file
        String modifiedOntologyFilePath = "/home/jules/TSP/CSC_Data/websemantique/Ontology/wine_populated.owl";
        writeModelToFile(model, modifiedOntologyFilePath);

        // Close model
        model.close();
    }
}
